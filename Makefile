CC = gcc
FLAGS = -Wall -o

GAME = game/shell.c game/tic_tac_toe_game.c

play_game:
	$(CC) $(FLAGS) play_game $(GAME)

clean:
	rm play_game