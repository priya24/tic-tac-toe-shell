
                [ What is this repository ? ]
--> This repository presents the tic-tac-toe shell implemented in C.



This file presents the logic behind the implementation for the tic-tic-toc
game.


--> The game is run by a tiny shell which can accepts the commands by players
    and invoke the corresponding game's module to process it.
    Please refer to instructions about specific command usage displayed
    when you run the executable for the first time.


                 [ How to run ? ]
--> Enter into the tic_tac_toe directory and run make.
    This should a build an executable called play_game in the same directory.
    To start the game, please run ./play_game.

--> To clean up the executable, please run make clean in the same directory.

                [ Maintaining the game state ]

--> The game board has 4 rows and 4 columns.

--> The game state is maintained in an unsigned integer of size 32 bits.
    (i.e. uint32_t typedef from stdint.h)

--> The status of every grid on the game board is maintained in 2 bits.
    Therefore, total of 16 grids can be maintained in a variable of size
    32 bits.

--> One byte of the game state variable represents one row of the game board.

--> The X move is encoded as bits 11.
--> The O move is encoded as bit 00.
--> The unset grids are encoded as bits 01. (Internal to game logic)

--> The bit manipulations are used to set a grid's 2 bit pattern to a
    particular game move's bit patterns.

                [ Checking for winner ]

--> To check if a particular move [ X | O ] is present horizontally in every
    column of a row, the game fetches the byte that represents the row status.
    If the byte has all 1's then we got the winner as X.
    If the byte has all 0's then we have winner as O.

--> To check if a winner exists along the columns, the game forms a byte
    by that has grid's encoded bits from every row from a column.

--> Similar logic applies for the diagonal grid.

                [ Validating the turn taking ]

--> Players take turn by choosing a grid location on the game board.
--> In every turn, either the player can set a chosen grid location to either
    [ X or O ] or can clear the previously set grid location.

