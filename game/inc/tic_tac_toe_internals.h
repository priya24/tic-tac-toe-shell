/** @file  tic_tac_toe_internals.h
 *
 *  @brief This file defines the commonly used macros which are internal
 *         to the tic-tac-toe game.
 *
 *  @author Priya Avhad
 *  @bug No Known Bugs
 */

#ifndef _GAME_INTERNALS_H
#define _GAME_INTERNALS_H

/**
 * @brief The symbol to denote the X move.
 */
#define X_MOVE 'X'

/**
 * @brief The symbol to denote the O move.
 */
#define O_MOVE 'O'

/**
 * @brief The symbol to denote an empty grid.
 */
#define EMPTY_GRID '-'

/**
 * @brief This macro defines the internal value used to represent an empty grid.
 */
#define EMPTY_GRID_VALUE 1

/**
 * @brief This macro defines the internal value used to represent the O move.
 */
#define O_MOVE_VALUE 0

/**
 * @brief This macro defines the internal value to represent the X move.
 */
#define X_MOVE_VALUE (( 1 << 1 ) | 1 )

/**
 * @brief The total number of moves in the game.
 *        The valid moves that can be played by a player are X or O.
 *        Remaining two moves are internals to the game logic.
 */
#define GAME_MOVES 4

/**
 * @brief The total number of rows in the game grid.
 */
#define GAME_ROWS 4

/**
 * @brief The total number of columns in the game grid.
 */
#define GAME_COLUMNS 4

/**
 * @brief The number of bits that represent one grid.
 */
#define GAME_GRID_SIZE 2

/**
 * @brief The mask to fetch the grid's value.
 */
#define GAME_GRID_MASK (( 1 << 1 ) | 1 )

/**
 * @brief The number of bits in a byte.
 */
#define BYTE_SIZE 8

/**
 * @brief The mask to initialize the game board.
 */
#define GAME_INIT_STATUS (( EMPTY_GRID_VALUE << 6 ) | \
                          ( EMPTY_GRID_VALUE << 4 ) | \
                          ( EMPTY_GRID_VALUE << 2 ) | \
                          ( EMPTY_GRID_VALUE )        \
                         )

#endif /* _GAME_INTERNALS_H */