/** @file  tic_tac_toe_game.h
 *
 *  @brief This function defines the public APIs of the tic-tac-toe game
 *         to be used by the tiny shell.
 *
 *  @author Priya Avhad
 *  @bug No Known Bugs
 */

#ifndef _TIC_TAC_TOE_GAME_H
#define _TIC_TAC_TOE_GAME_H

int init_game( game_info_t *game );

int display_game( game_info_t *game );

int perform_set( game_info_t *game,
                 uint32_t row,
                 uint32_t column,
                 char game_move );

int perform_clear( game_info_t *game, uint32_t row, uint32_t column );

#endif /* _TIC_TAC_TOE_GAME_H */