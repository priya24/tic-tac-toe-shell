#ifndef _GAME_INFO_H
#define _GAME_INFO_H

/** @file  game_info.h
 *
 *  @brief This file presents a game structure to be shared between tiny shell
 *         and game's logic.
 *         The purpose of this structure is to pass information about the
 *         game's status.
 *
 *  @author Priya Avhad
 *  @bug No Known Bugs
 */

#include <stdint.h>          /* For uint8_t datatype. */

/**
 * @brief The game's structure.
 */
typedef struct game_info {

    /**
     * @brief The number of rows on a game board.
     */
    uint8_t rows;

    /**
     * @brief The number of columns on a game board.
     */
    uint8_t columns;

    /**
     * @brief The current game state.
     */
    uint32_t game_state;

    /**
     * @brief The game's winning symbol. [ X or O ]
     */
    char game_winner;

    /**
     * @brief The boolean to check if the winner is found.
     */
    bool winner_found;


    /**
     * @brief The function to initialize the game board.
     */
    int (*init_game)( struct game_info *game );

    /**
     * @brief The function to display the game board on standout output.
     */
    int (*display_game)( struct game_info *game );

    /**
     * @brief The function to perform the processing of set command.
     */
    int (*perform_set)( struct game_info *game,
                        uint32_t row,
                        uint32_t column,
                        char game_move );

    /**
     * @brief The function to perform the processing of clear command.
     */
    int (*perform_clear)( struct game_info *game,
                          uint32_t row,
                          uint32_t column );
} game_info_t ;

#endif /* _GAME_INFO_H */