/** @file  tic_tac_toe_game.c
 *
 *  @brief This file presents the tic-tac-toe game's internal logic.
 *         It implements the processing of set and clear commands.
 *         It detects for the game's winner.
 *
 *  @author Priya Avhad
 *  @bug No Known Bugs
 */

/* -- Includes -- */
#include <stdio.h>                  /* For standard I/O library routines.*/
#include <assert.h>                 /* For assert().*/
#include <stdbool.h>                /* For bool datatype. */
#include <ctype.h>                  /* For toupper(). */

#include "inc/game_info.h"              /* For game_t structure declaration. */
#include "inc/tic_tac_toe_internals.h"  /*
                                         * For commonly used macros for
                                         * the game.
                                         */

/**
 * @brief This macro defines the error return code from a function invocation.
 */
#define ERROR -1

/**
 * @brief This macro defines the success return code from a function invocation.
 */
#define SUCCESS 0

/**
 * @brief This array defines the game moves (exposed to the players as well as
 *        internal to the game logic.)
 *        Every move in a game is encoded as certain bit pattern.
 *        This array serves the purpose of decoding a certain bit pattern to
 *        a particular game move's character.
 */
static char game_moves[GAME_MOVES];

/**
 * @brief This array defines the masks to fetch the status of a row from the
 *        game board.
 */
static uint32_t game_row_masks[GAME_ROWS];

/**
 * @brief The array is used to validated if the players are following the
 *        valid ordering in terms of taking turns.
 *        E.g. After a player 1 has set the move X, now it is the turn of
 *        player 2 set the move O.
 */
static char expected_moves[GAME_MOVES]= { EMPTY_GRID };

/**
 * @brief This is used to make sure the players are taking turns appropriately.
 */
static uint8_t turn = 0;

/**
 * @brief This function initializes the row masks.
 *
 * @details The purpose of row masks is fetch a status of a particular row of
 *          the game grid.
 *          Game state is maintained in a 32 bit unsigned integer.
 *          One byte of this integer represents one row from a game board.
 *
 * @param game The game to initialize.
 */
static void
initialize_game_masks( game_info_t *game ) {

    uint32_t row = 0;
    uint32_t column = 0;
    uint32_t row_mask = 0;

    /* Size of row in number of bits. */
    uint32_t row_size = game->columns * GAME_GRID_SIZE;

    /*
     * Prepare the row mask for the 0th least significant byte from an
     * integer that maintains the game state.
     */
    for( column = 0; column < game->columns; column++ ) {
        row_mask = (( row_mask << GAME_GRID_SIZE ) | GAME_GRID_MASK );
    }

    /*
     * Shift the row mask according to a particular row.
     */
    for( row = 0; row < game->rows; row++ ) {
        game_row_masks[row] = row_mask;
        row_mask = ( row_mask << row_size );
    }
}

/**
 * @brief The function initializes the game.
 *
 * @details Should be the first function to be invoked by the tiny shell.
 *
 * @param game  The game instance to initialize.
 * @return      0 on success and -1 on error.
 */
int
init_game( game_info_t *game ) {

    uint32_t row = 0;
    uint32_t game_init_status = 0;

    if( !game ) {
        return ERROR;
    }

    game->rows = GAME_ROWS;
    game->columns = GAME_COLUMNS;

    game_init_status = GAME_INIT_STATUS;

    /* Set the grids to empty grids. */
    for( row = 1; row < game->rows; row++ ) {
        game_init_status =
                (( game_init_status << BYTE_SIZE ) | GAME_INIT_STATUS );
    }

    game->game_state = game_init_status;

    game_moves[X_MOVE_VALUE] = X_MOVE;
    game_moves[O_MOVE_VALUE] = O_MOVE;
    game_moves[EMPTY_GRID_VALUE] = EMPTY_GRID;

    /* Initialize the row masks. */
    initialize_game_masks( game );

    return SUCCESS;
}

/**
 * @brief This function displays the game board's row on standard output.
 *
 * @param game The game instance
 * @param row  The row whose status is to be displayed.
 */
static void
display_game_row( game_info_t *game, uint32_t row ) {

    uint32_t grid_val = 0;
    uint32_t column = 0;

    uint32_t current_row_val = 0;

    uint32_t row_size = game->columns * GAME_GRID_SIZE;

    current_row_val = game->game_state & game_row_masks[row];

    current_row_val = current_row_val >> ( row * row_size );

    for( column = 0; column < game->columns; column++ ) {

        grid_val =
            current_row_val & ( GAME_GRID_MASK << ( column * GAME_GRID_SIZE ));

        grid_val = grid_val >> ( GAME_GRID_SIZE * column );

        printf( "\t\t%c", game_moves[grid_val] );
    }

    printf( "\n" );
}

/**
 * @brief This function displays the game board on standard output.
 *
 * @param game The game instance
 */
int
display_game( game_info_t *game ) {


    uint32_t row = 0;

    if( !game ) {
        return ERROR;
    }

    printf("\n\t\t\t Tic-Tac-Toe Game Board\n");

    printf("\t\t\t\t\t\t\t Next Player [%c]\n", expected_moves[ ( turn % 2 ) ]);

    printf(
    "\t-------------------------------------------------------------------\n");

    for( row = 0;  row < game->rows; row++ ) {
        display_game_row( game, row );
        printf(
        "\t-------------------------------------------------------------------\n");
    }

    return SUCCESS;
}

/**
 * @brief Fetches the status of a given row.
 *
 * @param game The game instance.
 * @param row  The target row whose status is to be fetched.
 *
 * @return     Returns the row status.
 *             It returns a 32 bit unsigned integer whose last bytes
 *             represents the row status.
 *             Remaining bytes are 0.
 */
static uint32_t
get_current_row_status( game_info_t *game, uint32_t row ) {

    uint32_t current_row_status = 0;
    uint32_t row_size = game->columns * GAME_GRID_SIZE;

    current_row_status = game->game_state & game_row_masks[row];

    current_row_status = current_row_status >> ( row * row_size );

    return current_row_status;
}

/**
 * @brief Returns the value of a particular grid.
 *
 * @details Every move in the game grid is encoded as a certain bit pattern.
 *          This function returns that bit patterns.
 *
 * @param game    The game instance.
 * @param row     Grid's X co-ordinate.
 * @param column  Grid's Y co-ordinate.
 *
 * @return The grid's characters bit patterns.
 */
static uint8_t
get_current_grid_value( game_info_t *game, uint32_t row, uint32_t column ) {

    uint32_t grid_value;

    uint32_t current_row_status = 0;

    current_row_status = get_current_row_status( game, row );

    grid_value =
       current_row_status  & ( GAME_GRID_MASK << ( column * GAME_GRID_SIZE ));

    grid_value = grid_value >> ( GAME_GRID_SIZE * column );

    return grid_value;
}

/**
 * @brief Set the grid to move X.
 *
 * @details The status of every grid on the game board is maintained in 2 bits.
 *          This function sets the 2 bits for the target grid to 11.
 *
 * @param game  The game instance.
 * @param row   Grid's X co-ordinate.
 * @param column Grid's Y co-ordinate.
 */
static void
set_grid_to_XMove( game_info_t *game, uint32_t row, uint32_t column ) {

    uint32_t new_grid_val = 0;
    uint32_t current_row_status = 0;
    uint32_t row_size = game->columns * GAME_GRID_SIZE;

    new_grid_val = X_MOVE_VALUE;

    new_grid_val = new_grid_val << ( GAME_GRID_SIZE * column );

    current_row_status = get_current_row_status( game, row );

    current_row_status |=  new_grid_val;

    current_row_status = current_row_status << ( row * row_size );

    game->game_state |=  current_row_status;
}

/**
 * @brief Set the grid to empty grid.
 *
 * @details The status of every grid on the game board is maintained in 2 bits.
 *          This function sets the 2 bits for the target grid to 01.
 *
 * @param game  The game instance.
 * @param row   Grid's X co-ordinate.
 * @param column Grid's Y co-ordinate.
 */
static void
set_grid_to_empty_Move( game_info_t *game, uint32_t row, uint32_t column ) {

    uint32_t new_grid_val = 0;
    uint32_t current_row_status = 0;
    uint32_t row_size = game->columns * GAME_GRID_SIZE;

    new_grid_val = EMPTY_GRID_VALUE;

    new_grid_val = new_grid_val << ( GAME_GRID_SIZE * column );

    current_row_status = get_current_row_status( game, row );

    current_row_status |=  new_grid_val;

    current_row_status = current_row_status << ( row * row_size );

    game->game_state |=  current_row_status;
}

/**
 * @brief Set the grid to move O.
 *
 * @details The status of every grid on the game board is maintained in 2 bits.
 *          This function sets the 2 bits for the target grid to 00.
 *
 * @param game  The game instance.
 * @param row   Grid's X co-ordinate.
 * @param column Grid's Y co-ordinate.
 */
static void
set_grid_to_OMove( game_info_t *game, uint32_t row, uint32_t column ) {

    uint32_t new_grid_val = 0;
    uint32_t row_size = game->columns * GAME_GRID_SIZE;

    new_grid_val = X_MOVE_VALUE;

    new_grid_val = new_grid_val << ( GAME_GRID_SIZE * column );

    new_grid_val = new_grid_val << ( row * row_size );

    new_grid_val = ~new_grid_val;

    game->game_state &=  new_grid_val;
}

/**
 * @brief Set the grid to move X.
 *
 * @details The status of every grid on the game board is maintained in 2 bits.
 *          This function sets the 2 bits for the target grid to a particular
 *          bit pattern..
 *
 * @param game  The game instance.
 * @param row   Grid's X co-ordinate.
 * @param column Grid's Y co-ordinate.
 *
 * @return      0 on success and -1 on error.
 */
static int
set_grid( game_info_t *game, uint32_t row, uint32_t column, char game_move ) {

    uint8_t grid_orig_value = 0;
    char grid_orig_move;

    grid_orig_value = get_current_grid_value( game, row, column );
    grid_orig_move = game_moves[grid_orig_value];

    if( grid_orig_move == EMPTY_GRID ) {

        if( game_move == X_MOVE ) {

            set_grid_to_XMove( game, row, column );
            return SUCCESS;

        } else if( game_move == O_MOVE ) {

            set_grid_to_OMove( game, row, column );
            return SUCCESS;
        }
    }
    return ERROR;
}

/**
 * @brief Depending upon the status of a particular row or column or
 *        diagonal, this function decides if the game's winner is found.
 *
 * @param game      The game instance.
 */
static void
check_game_status( game_info_t *game, uint8_t current_status ) {

    if( !current_status ) { /* Entire status marked with O move. */

        game->winner_found = true;
        game->game_winner = O_MOVE;
        return;

    } else {

        current_status = ~current_status;

        if( !current_status ) { /* Entire row marked with X move. */

            game->winner_found = true;
            game->game_winner = X_MOVE;
            return;
        }
    }
}

/**
 * @brief Check for a game winner in rows.
 *        This function sets the game status as winner found.
 *        Tiny shell can decide to continue the game if the winner has not
 *        reached.
 *
 * @param game      The game instance.
 */
static void
check_winner_for_rows( game_info_t *game ) {

    uint32_t row = 0;
    uint8_t current_row_status = 0;

    for( row = 0; row < game->rows; row++ ) {

        current_row_status = (uint8_t)get_current_row_status( game, row );
        check_game_status( game, current_row_status );
    }
}

/**
 * @brief Check for a game winner for columns.
 *        This function sets the game status as winner found.
 *        Tiny shell can decide to continue the game if the winner has not
 *        reached.
 *
 * @param game      The game instance.
 */
static void
check_winner_for_columns( game_info_t *game ) {

    uint32_t row = 0, column = 0;
    uint8_t current_column_status = 0;

    for( column = 0; column < game->columns; column++ ) {

        current_column_status = 0;

        for( row = 0; row < game->rows; row++ ) {

            current_column_status = current_column_status << GAME_GRID_SIZE;

            current_column_status |= get_current_grid_value( game, row, column );
        }

        check_game_status( game, current_column_status );
    }
}

/**
 * @brief Check for a game winner for diagonals.
 *        This function sets the game status as winner found.
 *        Tiny shell can decide to continue the game if the winner has not
 *        reached.
 *
 * @param game      The game instance.
 */
static void
check_winner_for_diagonals( game_info_t *game ) {

    uint8_t current_diag_status = 0;
    uint32_t row = 0, column = 0;

    while( (row < game->rows) && (column < game->columns) ) {

        current_diag_status = current_diag_status << GAME_GRID_SIZE;
        current_diag_status |= get_current_grid_value( game, row, column );
        row++;
        column++;
    }

    check_game_status( game, current_diag_status );
}

/**
 * @brief Check for a game winner.
 *        This function sets the game status as winner found.
 *        Tiny shell can decide to continue the game if the winner has not
 *        reached.
 *
 * @param game      The game instance.
 */
static void
check_winner( game_info_t *game ) {

    check_winner_for_rows( game );

    check_winner_for_columns( game );

    check_winner_for_diagonals( game );
}

/**
 * @brief This function validates if the players are following the
 *        valid ordering in terms of taking turns.
 *
 * @param game_move The gave move entered by the player.
 *
 * @return true if valid move and false otherwise.
 */
static bool
validate_current_move( char game_move ) {

    char expected_move;

    expected_move = expected_moves[ ( turn % 2 ) ];

    if( expected_move == EMPTY_GRID ) {

        if( game_move == X_MOVE ) {

            expected_moves[ ( turn % 2 ) ] = X_MOVE;
            expected_moves[ ( turn % 2 ) + 1 ] = O_MOVE;

        } else {

            expected_moves[ ( turn % 2 ) ] = O_MOVE;
            expected_moves[ ( turn % 2 ) + 1 ] = X_MOVE;

        }

        return true;

    } else {

       if( expected_move == game_move ) {
            return true;
       }
    }
    return false;
}

/**
 * @brief Performs processing of a set command.
 *
 * @param game      The game instance.
 * @param row       Grid's X co-ordinate.
 * @param column    Grid's Y co-ordinate.
 * @param game_move The gave mode entered by the player.
 *
 * @return      0 on success and -1 on error.
 */
int
perform_set( game_info_t *game,
                 uint32_t row,
                 uint32_t column,
                 char game_move ) {

    int retval = 0;

    if( !game ) {
        return ERROR;
    }

    if( row >= game->rows ) {
        printf("[ Invalid X Co-Ordinate ] \n");
        return ERROR;
    }

    if( column >= game->columns ) {
        printf("[ Invalid Y Co-Ordinate ]\n");
        return ERROR;
    }

    game_move = toupper( game_move );

    if(( game_move != X_MOVE) && ( game_move != O_MOVE)) {
        printf("[ Invalid game move ]\n");
        return ERROR;
    }

    if( !validate_current_move( game_move ) ) {

        printf("[ Wrong player turn ]\n");
        return ERROR;
    }

    retval = set_grid( game, row, column, game_move);
    if( retval == ERROR ) {
        printf("[ Invalid game move ]\n");
        return ERROR;
    }

    check_winner( game );

    turn++;

    return SUCCESS;
}


/**
 * @brief Performs processing of a clear command.
 *
 * @param game      The game instance.
 * @param row       Grid's X co-ordinate.
 * @param column    Grid's Y co-ordinate.
 *
 * @return      0 on success and -1 on error.
 */
int perform_clear( game_info_t *game,
                   uint32_t row,
                   uint32_t column ) {

    uint8_t grid_orig_value;
    char grid_orig_move;

    grid_orig_value = get_current_grid_value( game, row, column );
    grid_orig_move = game_moves[grid_orig_value];

    if( grid_orig_move == EMPTY_GRID ) {
        return SUCCESS;
    }

    if( !validate_current_move( grid_orig_move ) ) {

        printf("[ Wrong player turn ]\n");
        return ERROR;
    }

    set_grid_to_OMove( game, row, column );

    set_grid_to_empty_Move( game, row, column );

    turn++;
    return SUCCESS;
}