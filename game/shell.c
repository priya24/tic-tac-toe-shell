/** @file  shell.c
 *
 *  @brief This file presents a tiny shell that runs the tic tac toe game.
 *
 *         It pareses the commands and invokes the corresponding the game's
 *         module to process the command.
 *
 *  @author Priya Avhad
 *  @bug No Known Bugs
 */

/* -- Includes -- */
#include <stdio.h>                  /* For standard I/O library routines.*/
#include <stdlib.h>                 /* For exit(). */
#include <stdbool.h>                /* For true/false. */
#include <stdlib.h>                 /* For NULL. */
#include <string.h>                 /* For strtok(). */

#include "inc/game_info.h"          /* For game_into_t structure declaration. */

#include "inc/tic_tac_toe_game.h"   /* For tic tac toe game APIs. */

/**
 * @brief This macro defines the set command.
 *        The set command is used to set a move at a grid location.
 */
#define SET_COMMAND "set"

/**
 * @brief This macro defines the clear command.
 *        The clear command is used to clear a previously set grid location.
 */
#define CLEAR_COMMAND "clear"

/**
 * @brief This macro defines the quit command.
 *        The quit command is used to quit the game.
 */
#define QUIT_COMMAND "quit"

/**
 * @brief This macro defines the help command.
 *        The help command is used to get a help regrading the game.
 */
#define HELP_COMMAND "help"

/**
 * @brief This macro defines the error return code from a function invocation.
 */
#define ERROR -1

/**
 * @brief This macro defines the success return code from a function invocation.
 */
#define SUCCESS 0

/**
 * @brief This macro defines the maximum length of a command accepted
 *        by this shell.
 */
#define MAXLINE_TSH 1024

/**
 * @brief The tiny shell's command prompt.
 */
static char prompt[] = "tsh>";          /* Common line prompt. */

/**
 * @brief This function displays the game's exit screen.
 */
static void
exit_screen() {
    printf("\n\t-------- Thank you for playing ! --------\n\n");
}

/**
 * @brief This function displays the game's help screen.
 */
static void
instruction_screen() {

    printf(
    "\t-------------------------------------------------------------------\n");

    printf("\t Welcome to Tic Tac Toe Game.\n\n");
    printf("\t This is an interactive game between two players.\n");

    printf("\n\t\t\t\t [ Taking Turns ]\n");
    printf(
    "\t Players take turn by choosing a grid location on the game board.\n");

    printf(
    "\t In every turn, player sets a chosen grid location to either X or O.\n");

    printf(
    "\t OR In every turn, player can clear a previously set grid.\n");

    printf("\n\t\t\t\t [ Winning ]\n");
    printf("\t The first player who successfully places the chosen symbol\n");
    printf(
    "\t aligned vertically / horizontally / diagonally wins the game.\n");

    printf("\n\t\t\t\t [ Commands ]\n");
    printf("\t 1. set [ X | O ] [ position ]  --> Set the grid location.\n");

    printf(
    "\t 2. clear [ position ]          --> Clears the previously set location.\n");

    printf("\t 3. quit                        --> Quits the game.\n");

    printf(
        "\t 4. help                        --> To get this help again. \n\n");

    printf("\t Note : \n");
    printf(
    "\t Position  --> [ X Y ] where X and Y are zero based coordinates.\n");

    printf("\t E.g. set X 0 0  OR set O <english O> 3 3 \n\n");

    printf("\t In every turn, player can either set an empty grid or clear\n");
    printf("\t the previously set grid by himself/herself.\n");

    printf(
    "\t-------------------------------------------------------------------\n");
}

/**
 * @brief This function initializes the game.
 *
 * @param game The game to initialize.
 * @return     0 on success and -1 on error.
 */
static void
shell_init_game( game_info_t *game ) {

    int retval = 0;

    retval = game->init_game( game );
    if( retval == ERROR ) {
        printf("[ Error in initializing the tic-tac-toe game. ]");
         exit( ERROR );
    }
}

/**
 * @brief This function displays the game board on screen.
 *
 * @param game The game to display.
 * @return     0 on success and -1 on error.
 */
static void
shell_display_game( game_info_t *game ) {

    int retval = 0;

    retval = game->display_game( game );
    if( retval == ERROR ) {
        printf("[ Error in displaying the tic-tac-toe game. ]");
         exit( ERROR );
    }
}

/**
 * @brief This function invokes the game's the set command processing.
 *
 * @param game      The game.
 * @param row       Grid's X co-ordinate.
 * @param column    Grid's Y co-ordinate.
 * @param game_move The [ X | O ] move.
 *
 * @return 0 on success and -1 on error.
 */
static int
shell_perform_set( game_info_t *game,
                   uint32_t row,
                   uint32_t column,
                   char game_move ) {

    return game->perform_set( game, row, column, game_move );
}

/**
 * @brief This function invokes the game's the clear command processing.
 *
 * @param game   The game.
 * @param row    Grid's X co-ordinate.
 * @param column Grid's Y co-ordinate.
 *
 * @return 0 on success and -1 on error.
 */
static int
shell_perform_clear( game_info_t *game,
                     uint32_t row,
                     uint32_t column ) {

    return game->perform_clear( game, row, column );
}

/**
 * @brief This function parses the command and invokes the corresponding
 *        game's module to process the command.
 *
 * @param game      The game.
 * @param command   The command to parse and process.
 *
 * @return 0 on success and -1 on error.
 */
static int
evaluate_command( game_info_t *game, char *cmdline ) {

    char *command = NULL;
    char *cmd_tok = NULL;
    const char *delimiter = " ";

    uint32_t row = 0;
    uint32_t column = 0;
    char game_move;

    /* Read the command type. */
    cmd_tok = strtok( cmdline, delimiter );

    if( !cmd_tok ) {
        return ERROR;
    }

    if(!strcmp( cmd_tok, SET_COMMAND ) || !strcmp( cmd_tok, CLEAR_COMMAND )){

        command = cmd_tok;

        if( !strcmp( command, SET_COMMAND ) ) {

            /* Get the [ X | O ] move. */
            cmd_tok = strtok( NULL, delimiter );
            if( !cmd_tok ) {
                printf("[ Invalid command. ]\n");
                return ERROR;
            }

            game_move = *cmd_tok;
        }

        /* Get the grid's X co-ordinate. */
        cmd_tok = strtok( NULL, delimiter );
        if( !cmd_tok ) {
            printf("[ Invalid command. ]\n");
            return ERROR;
        }
        row = atoi( cmd_tok );

        /* Get the grid's Y co-ordinate. */
        cmd_tok = strtok( NULL, delimiter );
        if( !cmd_tok ) {
            printf("[ Invalid command. ]\n");
            return ERROR;
        }
        column = atoi( cmd_tok );

        if( !strcmp( command, SET_COMMAND ) ) {

            return shell_perform_set( game, row, column, game_move );

        } else {

            return shell_perform_clear( game, row, column );
        }

    } else if ( !strcmp( cmd_tok, QUIT_COMMAND ) ) {

        exit_screen();
        exit( SUCCESS );

    } else if ( !strcmp( cmd_tok, HELP_COMMAND ) ) {

        instruction_screen();
        return SUCCESS;
    } else {

        printf("[ Invalid command. ]\n");
        return ERROR;
    }
}

/**
 * @brief Tiny shell's main routine.
 *
 * @return 0 on success and -1 on error.
 */
int
main() {

    char cmdline[MAXLINE_TSH + 1];    /* cmdline for fgets */

    game_info_t current_game = {

        .winner_found = false,

        .init_game = init_game,

        .display_game = display_game,

        .perform_set = perform_set,

        .perform_clear = perform_clear
    };

    shell_init_game( &current_game );

    instruction_screen();

    while( true ) {

        shell_display_game( &current_game );

        /* Emit the shell prompt. */
        printf( "%s", prompt );

        if ((fgets( cmdline, MAXLINE_TSH, stdin) == NULL ) && ferror( stdin )) {
            printf( "[ Error in getting player's command. ]" );
            return ERROR;
        }

        if ( !strlen( cmdline ) ) {
            continue;
        }

        /* Remove the trailing newline */
        cmdline[strlen(cmdline)-1] = '\0';

        /* Parse and process the command. */
        if( evaluate_command( &current_game, cmdline ) == ERROR ) {
            continue;
        }

        if( current_game.winner_found ) {

            /* Winner found. */
            shell_display_game( &current_game );

            printf( "\nCongratulations ! Winner is %c\n\n",
                    current_game.game_winner );
            exit_screen();
            exit( SUCCESS );
        }

    }
    return SUCCESS;
}